import openpyxl
import shutil
import os
from shutil import move
from tempfile import NamedTemporaryFile

import xlwt

from enums.testrail import VAVFTestFields
import pandas
import xlsxwriter
from enums.testrail import TestResultStatus

import logging
LOGGER = logging.getLogger(__name__)


class FileUtils():
    columns = ['test_id', 'custom_actual_result', 'custom_va_response', 'status_id', 'custom_intent_confidence', 'custom_intent_domain']

    def create_empty_folder(self, foldername):
        #LOGGER.info(foldername)
        if os.path.exists(foldername):
            os.chmod(foldername, 0o777)
            shutil.rmtree(foldername)
        os.makedirs(foldername, 0o777)

    def remove_file(self, filename):
        if os.path.exists(filename):
            os.remove(filename)
        else:
            print('File does not exists')

    # Append list to a file
    def append_list_to_file(self, filename, input_lists, type="list"):
        if type == "string":
            input_lists = [input_lists]
        if os.path.exists(filename):
            file = open(filename, 'a', encoding="utf-8")
        else:
            file = open(filename, 'w+', encoding="utf-8")
        for i in input_lists:
            if i != '' and i is not None:
                file.write("%s\n" % (i))
        file.close()

    # Check if file is existed or not
    def file_is_exist(self, file_name):
        return os.path.exists(file_name)

    # Append s ring to a file
    def append_to_file(self, filename, string):
        if os.path.exists(filename):
            file = open(filename, 'a', enoding="utf-8")
        else:
            file = open(filename, 'w+', encoding="utf-8")
        if string != '' and string is not None:
            file.write("%s\n" % string)
        file.close()

    def get_current_dirname(self):
        dirname, runname = os.path.split(os.path.abspath(__file__))
        #LOGGER.info(dirname)
        return dirname

    def get_file_path(self, file_source):
        return os.path.dirname(os.path.realpath(file_source))

    # Copy file to other folder
    def copy_file(self, file_source, file_name_destination, folder_destination = None):
        if folder_destination == None:
            file_name_full = os.path.dirname(os.path.realpath(file_source)) + r'\\' + file_name_destination
        else:
            file_name_full = folder_destination + r'\\' + file_name_destination
        if not self.file_is_exist(file_name_full):
            shutil.copy(file_source, file_name_full, follow_symlinks=True)
        return file_name_full

    def remove_first_line_in_file(self, filename):
        temp_path = None
        with open(filename, 'r', encoding='utf-8') as f_in:
            with NamedTemporaryFile(mode='w', delete=False, encoding='utf-8') as f_out:
                temp_path = f_out.name
                next(f_in)  # skip first line
                for line in f_in:
                    f_out.write(line)
        os.remove(filename)
        move(temp_path, filename)

    def clear_content_file(self, file_name):
        with open(file_name, 'a', encoding='utf-8') as file:
            file.truncate(0)

    # Get number of rows in excel file
    def get_number_of_rows_in_excel_file(self, file_source):
        df = pandas.read_excel(file_source)
        number = len(df.index)
        return number

    # Create excel file if not existed
    def open_xlsx_file(self, file_name):
        if not self.file_is_exist(file_name):
            workbook = xlsxwriter.Workbook(file_name)
            worksheet = workbook.add_worksheet()
            worksheet.write(0, 0, VAVFTestFields.TEST_ID.value)
            worksheet.write(0, 1, VAVFTestFields.GROUP_COMMAND.value)
            worksheet.write(0, 2, VAVFTestFields.DESCRIPTION.value)
            worksheet.write(0, 3, VAVFTestFields.PRECONDITION.value)
            worksheet.write(0, 4, VAVFTestFields.COMMAND.value)
            worksheet.write(0, 5, VAVFTestFields.EXPECTED_RESULT.value)
            worksheet.write(0, 6, VAVFTestFields.ACTUAL_RESULT.value)
            worksheet.write(0, 7, VAVFTestFields.RESPONSE.value)
            worksheet.write(0, 8, VAVFTestFields.STATUS.value)
            worksheet.write(0, 9, VAVFTestFields.DOMAIN.value)
            worksheet.write(0, 10, VAVFTestFields.INTENT.value)
            workbook.close()
        writer = pandas.ExcelWriter(file_name, engine='openpyxl', mode='a', if_sheet_exists='replace')
        return writer

    def convert_xlsx_to_xls_file(self, file_name):
        file_name_xls = file_name.replace(".xlsx", ".xls")
        wb = openpyxl.load_workbook(file_name)
        wb.save(file_name_xls)
        return file_name_xls

    # Remove from excel file
    def delete_from_xls(self, file_name, number, first_row=1):
        # Read from file
        df = pandas.read_excel(file_name)
        # Delete range of rows from rows 1 to rows [number]
        df = df.drop(df.index[int(first_row):int(number)]) # Not delete the header
        df.set_index('ID', inplace=True)
        # Rewrite to file
        df.to_excel(file_name, engine='xlsxwriter')

    # Read rows from xls file by pandas
    def read_rows_from_excel(self, file_name, number, fill_na='None'):
        if not number:
            df = pandas.read_excel(file_name)
        else:
            df = pandas.read_excel(file_name, nrows=int(number))
        # Drop row which all cells are NaN
        df = df.dropna(how='all')
        # Change nan to another string
        df.fillna(fill_na, inplace=True)
        return df

    # Read test report (.xls) to dictionary with [number] items
    def read_test_result_from_xls(self, file_name, number=100, relative_result=False):
        df = self.read_rows_from_excel(file_name, number, 'None')
        # Convert test result
        df = self.set_status_id(df, relative_result)
        # Convert data to correct format
        df = df.drop("2_Group Command", 1)
        df = df.drop("3_Description", 1)
        df = df.drop("4_Preconditions", 1)
        df = df.drop("5_Command (Tiếng Việt)", 1)
        df = df.drop("6_Expected Result", 1)
        # Rename column
        df = df.rename(columns={'ID': 'test_id', 'Actual Result': 'custom_actual_result',
                                'Response': 'custom_va_response', 'Result Status': 'status_id',
                                'Intent & Confidence': 'custom_intent_confidence',
                                'Intent Domain': 'custom_intent_domain'})
        df['test_id'] = df['test_id'].str.replace(r'T', '')
        df['status_id'] = df['status_id'].str.replace(r'PASS', TestResultStatus.PASSED.value)
        df['status_id'] = df['status_id'].str.replace(r'FAIL', TestResultStatus.FAILED.value)
        df['status_id'] = df['status_id'].str.replace(r'None', TestResultStatus.RETEST.value) # Incase no result, means N/A
        data = df.to_dict('records')
        LOGGER.info(data)
        return data

    # Set result id
    # If relative_result = False, get the current result from test result
    # If relative_result = True, recheck and get relative result
    def set_status_id(self, df, relative_result=False):
        row, col = df.shape
        LOGGER.info(row)
        if relative_result:
            for i in range(row):
                actual_result = df._get_value(i, 'Actual Result')
                expect_result = df._get_value(i, '6_Expected Result')
                list_expect_result = expect_result.split('##')
                status_result = all(x in actual_result for x in list_expect_result)
                if status_result:
                    df.at[i, 'Result Status'] = 'PASS'
        return df

    # Read sample from xls file
    def read_from_xls(self, file_name, number=None):
        df = self.read_rows_from_excel(file_name, number, 'None')
        data = df.to_dict('records')
        return data

    # Write sample to xls file
    def write_test_result_to_xlsx(self, file_name, dict_data):
        LOGGER.info(dict_data)
        sheetname = "Sheet1"
        # Create file if not exist
        writer = self.open_xlsx_file(file_name)
        reader = pandas.read_excel(file_name)
        #df = pandas.DataFrame.from_dict(dict_data)
        LOGGER.info(dict_data)
        #dict_data = pandas.Series(dict_data).to_frame()
        LOGGER.info(dict_data)
        LOGGER.info(type(dict_data))
        #df = pandas.DataFrame.from_dict(dict_data, orient='tight', columns=["test_id", "custom_command", "custom_vavf_description", "custom_vavf_precondition", "custom_vavf_command", "custom_vavf_expected", "custom_actual_result", "custom_va_response", "status_id", "custom_intent_domain", "custom_intent_confidence"])
        df = pandas.DataFrame.from_records(dict_data, index=[0])
        #df.set_index(VAVFTestFields.TEST_ID.value, inplace=True)
        #LOGGER.info(writer.sheets[sheetname].max_row)
        #LOGGER.info(reader.columns)
        LOGGER.info(len(reader)+1)
        #df = pandas.DataFrame(dict_data, index="test_id")
        LOGGER.info(df)
        df.to_excel(writer, index=False, header=False,startrow=len(reader)+1, columns=reader.columns)

    # Read log file until found string
    def parse_log(self, file_name, found):
        with open(file_name) as f:
            while True:
                line = f.readline()
                if line.find(found):
                    print(line)



