import os
import time
from config.sample import AUDIO_WAKE_UP
from playsound import playsound


class MediaUtils():

    def file_is_exist(self, file_name):
        return os.path.exists(file_name)

    # Play mp3 file
    def play_an_audio(self, audio_name, wait=1):
        if not self.file_is_exist(audio_name):
            return None
        playsound(audio_name, block=True)
        time.sleep(wait)

