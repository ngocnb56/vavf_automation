import logging
import os
import re
import time

from requests import ReadTimeout
from retry import retry
from testrail_api import TestRailAPI, StatusCodeError

import config.common as testrail_cfg
import myutils.utils as utils
from enums.testrail import TestFields
from models.api.objects.language_objects import LanguageObjects

LOGGER = logging.getLogger(__name__)


class TestrailUtils:
    def __init__(self, get_params):
        # self.username = get_params["username"]
        self.update_test_case = None
        self.username = get_params.get("username")
        self.password = get_params.get("password")
        self.target_language = get_params.get("target_language")
        self.testing_type = get_params.get("testing_type")
        self.relative_result = get_params.get("relative_result")
        self.dictionary_file = get_params.get("dictionary_file")
        self.testrail = TestRailAPI(testrail_cfg.TESTRAIL_URL, self.username, self.password)
        if self.dictionary_file is not None:
            self.language = LanguageObjects(get_params)
        # self.skype = SkypeLocalUtils()

    @staticmethod
    def access_testrail(username, password):
        testrail = TestRailAPI(testrail_cfg.TESTRAIL_URL, username, password)
        # Validate authenticate
        try:
            testrail.projects.get_project(testrail_cfg.TESTRAIL_PROJECT_ID)
        except StatusCodeError:
            LOGGER.info("Incorrect email / password for testrail. Please kindly check again!")
            import os
            os._exit(0)
        return testrail

    def set_run_id_info(self, run_id):
        # Temp
        self.run_id = run_id
        run_info = self.get_run_id_info(self.run_id)
        self.run_id_config = utils.get_value_from_dict(run_info, "config", "")
        # Get project name
        self.project_id = utils.get_value_from_dict(run_info, "project_id", "")
        project_info = self.__get_project_info(self.project_id)
        self.project_name = utils.get_value_from_dict(project_info, "name", "")
        # Get suite infor
        self.suite_id = utils.get_value_from_dict(run_info, "suite_id", "")
        # Get run_id name
        run_id_name = run_info["name"]
        if self.run_id_config == "" or self.run_id_config == None:
            run_id_name = utils.convert_list_to_string([self.run_id, run_id_name], "_")
        else:
            run_id_name = utils.convert_list_to_string([self.run_id, run_id_name, self.run_id_config], "_")
        run_id_name = utils.remove_special_character(run_id_name)
        self.run_id_name = run_id_name
        # Get milestone
        milestone_id = utils.get_value_from_dict(run_info, "milestone_id", "")
        milestone_name = ""
        if milestone_id is not None:
            milestone_info = self.__get_milestone_info(milestone_id)
            if milestone_info is not None:
                milestone_name = milestone_info["name"]
        self.milestone_name = milestone_name

    def get_run_id_config(self):
        return self.run_id_config

    def get_project_id(self):
        return self.project_id

    def get_project_name(self):
        return self.project_name

    def get_suite_id(self):
        return self.suite_id

    def get_run_id_name(self):
        return self.run_id_name

    def get_milestone_name(self):
        return self.milestone_name

    def set_update_test_case(self, update_test_case):
        self.update_test_case = update_test_case
        try:
            self.case_id = self.update_test_case[TestFields.TEST_ID.value]
        except KeyError:
            # If no test_id, get case_id
            self.case_id = self.update_test_case[TestFields.CASE_ID.value]
        self.current_test_case = self.__get_case_by_case_id(self.case_id)

    def get_test_ids_from_run_id(self, run_id):
        list_of_test_ids = []
        for each_test in self.testrail.tests.get_tests(run_id=run_id):
            list_of_test_ids.append(each_test.get('id'))
        return list_of_test_ids

    def get_test_plan_info(self, plan_id):
        return self.testrail.plans.get_plan(plan_id=plan_id)

    @retry((StatusCodeError, ReadTimeout, ConnectionError, StatusCodeError), tries=5, delay=30)
    def __get_test_run_info(self, run_id):
        return self.testrail.runs.get_run(run_id=run_id)

    @retry((StatusCodeError, ReadTimeout, ConnectionError, StatusCodeError), tries=5, delay=30)
    def __get_project_info(self, project_id):
        return self.testrail.projects.get_project(project_id=project_id)

    @retry((ReadTimeout, ConnectionError), tries=5, delay=30)
    def __get_milestone_info(self, milestone_id):
        try:
            return self.testrail.milestones.get_milestone(milestone_id=milestone_id)
        except StatusCodeError:
            return None

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def get_test_id_info(self, test_id):
        self.testrail.tests.get_test(test_id=test_id)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __get_case_by_case_id(self, case_id):
        return self.testrail.cases.get_case(case_id)

    # Get test by ID
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def get_test_by_test_id(self, test_id):
        return self.testrail.tests.get_test(test_id)

    # Add a new section
    def __add_section(self, project_id, suite_id, section):
        return self.testrail.sections.add_section(project_id=project_id,
                                                  suite_id=suite_id,
                                                  parent_id=section["parent_id"],
                                                  name=section["name"])

    # # Get test by ID
    # @retry((StatusCodeError, ReadTimeout), tries=3, delay=20)
    # def get_result_by_test_id(self, test_id):
    #     return self.testrail.results.get_results(test_id)

    # Get results by run ID
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def get_results_for_run_by_run_id(self, run_id, offset=0, status_id=None):
        list_test = self.testrail.results.get_results_for_run(run_id, offset=offset, status_id=status_id)
        return list_test

    # Get suites by project ID
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __get_suites_by_project_id(self, project_id):
        list_suites = self.testrail.suites.get_suites(project_id)
        return list_suites

    # Get suites by project ID
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def get_resutls_by_test_id(self, test_id):
        list_results = self.testrail.results.get_results(test_id, limit=20)
        return list_results

    # # Get results by test ID
    # @retry((StatusCodeError, ReadTimeout), tries=3, delay=20)
    # def get_results_by_test_id(self, test_id):
    #     list_results = self.testrail.results.get_results(test_id)
    #     return list_results

    # Get sections by project_id and suite_id
    @retry((StatusCodeError, ReadTimeout), tries=5, delay=30)
    def get_sections_by_project_id(self, project_id, suite_id, offset):
        list_sections = self.testrail.sections.get_sections(project_id=project_id, suite_id=suite_id, offset=offset)
        return list_sections

    # Status_id = None: Get all cases
    # status_id = TestResultStatus.PASSED.value: Get only PASSED test case
    # status_id = TestResultStatus.BLOCKED.value: Get only BLOCKED test case
    # status_id = TestResultStatus.UNTESTED.value: Get only UNTESTED test case
    # status_id = TestResultStatus.RETEST.value: Get only RETEST test case
    # status_id = TestResultStatus.FAILED.value: Get only FAILED test case
    # custom_vavf_precondition = "Empty": Only check case which have no precondition
    @retry((StatusCodeError, ConnectionError, TimeoutError), tries=5, delay=30)
    def get_tests_by_run_id(self, run_id, offset=0, status_id=None,
                            custom_vavf_precondition=None):
        list_test = self.testrail.tests.get_tests(run_id, offset=offset, status_id=status_id)
        if custom_vavf_precondition is not None:
            for item in list_test["tests"][:]:
                if not utils.check_if_substring_exists(utils.convert_encoding(item[TestFields.VA_PRECONDITION.value]),
                                                       custom_vavf_precondition):
                    list_test["tests"].remove(item)
        return list_test

    @retry((StatusCodeError, ConnectionError, TimeoutError), tries=5, delay=30)
    def __get_cases_by_suite_id(self, project_id, suite_id, offset=0, filter=""):
        list_test = self.testrail.cases.get_cases(project_id=project_id, suite_id=suite_id, offset=offset,
                                                  filter=filter)
        return list_test

    # Get run_id properties by key
    def get_run_id_key(self, run_id, key_name):
        run_id_info = self.testrail.runs.get_run(run_id)
        return run_id_info[key_name]

    @retry(StatusCodeError, tries=5, delay=30)
    def get_list_run_id_by_plan_id(self, plan_id):
        list_run_id = []
        plan = self.testrail.plans.get_plan(plan_id)
        for run_id in plan["entries"]:
            for i in range(len(run_id["runs"])):
                list_run_id.append(run_id["runs"][i]["id"])
        return list_run_id

    @retry(StatusCodeError, tries=5, delay=30)
    def get_list_run_id_by_milestone_id(self, milestone_id):
        list_run_id = []
        milestone = self.testrail.milestones.get_milestone(milestone_id)
        project_id = milestone["project_id"]
        run_info = self.testrail.runs.get_runs(project_id, milestone_id=milestone_id)
        plan_info = self.testrail.plans.get_plans(project_id, milestone_id=milestone_id)
        for run_id in run_info["runs"]:
            list_run_id.append(run_id["id"])
        for plan_id in plan_info["plans"]:
            list_run_id.extend(self.get_list_run_id_by_plan_id(plan_id["id"]))
        return list_run_id

    # Set test result by test id for only VFVA template
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __add_result_by_test_id_vfva_template(self, test_result):
        self.delay()
        self.testrail.results.add_result(test_result[TestFields.TEST_ID.value],
                                         custom_actual_result=test_result[TestFields.VA_ACTUAL_RESPONSE.value],
                                         custom_va_response=test_result[TestFields.VA_TTS_RESPONSE.value],
                                         status_id=test_result[TestFields.STATUS.value],
                                         custom_intent_domain=test_result[TestFields.VA_ACTUAL_DOMAIN.value],
                                         custom_intent_confidence=test_result[
                                             TestFields.VA_ACTUAL_INTENT_CONFIDENCE.value],
                                         comment=test_result[TestFields.COMMENT.value])

    # Set test result by id for only VFVA template
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __add_result_by_id_vfva_template(self, test_result):
        self.delay()
        self.testrail.results.add_result(test_result[TestFields.ID.value],
                                         custom_actual_result=test_result[TestFields.ACTUAL_RESULT.value],
                                         custom_va_response=test_result[TestFields.VA_TTS_RESPONSE.value],
                                         status_id=test_result[TestFields.STATUS.value],
                                         custom_intent_domain=test_result[TestFields.VA_ACTUAL_DOMAIN.value],
                                         custom_intent_confidence=test_result[
                                             TestFields.VA_ACTUAL_INTENT_CONFIDENCE.value],
                                         comment=test_result[TestFields.COMMENT.value])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __add_result_by_test_id_text_template(self, test_result):
        self.delay()
        self.testrail.results.add_result(test_result[TestFields.TEST_ID.value],
                                         status_id=test_result[TestFields.STATUS.value],
                                         comment=test_result[TestFields.COMMENT.value])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __add_result_by_id_text_template(self, test_result):
        self.delay()
        self.testrail.results.add_result(test_result[TestFields.ID.value],
                                         status_id=test_result[TestFields.STATUS.value],
                                         comment=test_result[TestFields.COMMENT.value])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def add_test_results_by_run_id(self, run_id, test_result):
        self.delay()
        return self.testrail.results.add_results(run_id, test_result)

    # Set test result by test id for only VSS template
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def add_result_by_test_id_vss_template(self, test_result):
        self.delay()
        self.testrail.results.add_result(test_result[TestFields.ID.value],
                                         custom_actual_result=test_result[TestFields.ACTUAL_RESULT.value],
                                         custom_va_response=test_result[TestFields.VA_TTS_RESPONSE.value],
                                         status_id=test_result[TestFields.STATUS.value],
                                         custom_intent_domain=test_result[TestFields.VA_ACTUAL_DOMAIN.value],
                                         custom_intent_confidence=test_result[
                                             TestFields.VA_ACTUAL_INTENT_CONFIDENCE.value],
                                         comment=test_result[TestFields.COMMENT.value])

    # Set test result by test id for Dialog Template
    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def add_result_by_test_id_dialog_template(self, test_result):
        self.delay()
        self.testrail.results.add_result(test_result[TestFields.ID.value],
                                         custom_actual_dialog=test_result[TestFields.ACTUAL_DIALOG.value],
                                         custom_actual_dialog_voice=test_result[TestFields.ACTUAL_DIALOG_VOICE.value],
                                         custom_actual_entity=test_result[TestFields.ACTUAL_ENTITY.value],
                                         custom_actual_intent=test_result[TestFields.ACTUAL_INTENT.value],
                                         # custom_actual_domain=test_result[TestFields.ACTUAL_DOMAIN.value],
                                         status_id=test_result[TestFields.STATUS.value],
                                         defects=test_result[TestFields.DEFECT.value],
                                         custom_pic=test_result[TestFields.PIC.value],
                                         custom_pic_note=test_result[
                                             TestFields.PIC_COMMENT.value],
                                         comment=test_result[TestFields.COMMENT.value])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_vavf_expected(self, custom_vavf_expected):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_expected=custom_vavf_expected)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_defect(self, custom_defect):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_defect=custom_defect)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_vavf_expected_en(self, custom_vavf_expected_en):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_expected_en=custom_vavf_expected_en)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_vavf_expected_en(self, custom_vavf_expected_en):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_expected_en=custom_vavf_expected_en)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_vavf_expected_intent(self, custom_vavf_expected_intent):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_expected_intent=custom_vavf_expected_intent)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_vavf_expected_intent_en(self, custom_vavf_expected_intent_en):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_expected_intent_en=custom_vavf_expected_intent_en)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_vavf_expected_va_command(self, custom_vavf_expected_command):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id,
                                               custom_vavf_expected_command=custom_vavf_expected_command)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_vavf_expected_device(self, custom_vavf_expected_device):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_expected_device=custom_vavf_expected_device)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_title(self, title):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, title=title[:250])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_sample(self, custom_nlp_sample):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_nlp_sample=custom_nlp_sample,
                                               title=str(custom_nlp_sample)[:250])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_sample_en(self, custom_nlp_sample_en, update_title=False):
        self.delay_time()
        if update_title:
            return self.testrail.cases.update_case(self.case_id, custom_nlp_sample_en=custom_nlp_sample_en,
                                                   title=str(custom_nlp_sample_en)[:200])
        else:
            return self.testrail.cases.update_case(self.case_id, custom_nlp_sample_en=custom_nlp_sample_en)
        # return self.testrail.cases.update_case(self.case_id, custom_nlp_sample_en=custom_nlp_sample_en, title=str(custom_nlp_sample_en)[:200])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_sample_fr(self, custom_nlp_sample_fr):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_nlp_sample_fr=custom_nlp_sample_fr)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_audioname(self, custom_audioname):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_audioname=custom_audioname)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_domain(self, custom_domain):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_nlp_expected_domain=custom_domain)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_intent(self, custom_nlp_expected_intent):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_nlp_expected_intent=custom_nlp_expected_intent)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_note(self, custom_note):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_note=custom_note)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def update_test_case_custom_entity(self, custom_nlp_expected_entity_value):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id,
                                               custom_nlp_expected_entity_value=custom_nlp_expected_entity_value)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_dialog(self, custom_nlp_expected_dialog):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_nlp_expected_dialog=custom_nlp_expected_dialog)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_dialog_en(self, custom_nlp_expected_dialog_en):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id,
                                               custom_nlp_expected_dialog_en=custom_nlp_expected_dialog_en)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_dialog_fr(self, custom_nlp_expected_dialog_fr):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id,
                                               custom_nlp_expected_dialog_fr=custom_nlp_expected_dialog_fr)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_dialog_voice(self, custom_nlp_expected_dialog_voice):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id,
                                               custom_nlp_expected_dialog_voice=custom_nlp_expected_dialog_voice)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_asr(self, custom_asr_expected):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_asr_expected=custom_asr_expected)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_preconds(self, custom_preconds):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_preconds=custom_preconds)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_steps_text(self, custom_steps):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_steps=custom_steps)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_steps_step(self, custom_steps_separated):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_steps_separated=custom_steps_separated)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_expected(self, custom_expected):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_expected=custom_expected)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_custom_note(self, custom_note):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_note=custom_note)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_precondition(self, custom_vavf_precondition):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_precondition=custom_vavf_precondition)

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_command_vn(self, custom_vavf_command):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_command=str(custom_vavf_command),
                                               title=str(custom_vavf_command)[:250])

    @retry((StatusCodeError, ReadTimeout, ConnectionError), tries=5, delay=30)
    def __update_test_case_command_en(self, custom_vavf_command_en):
        self.delay_time()
        return self.testrail.cases.update_case(self.case_id, custom_vavf_command_en=str(custom_vavf_command_en))

    def delete_test_case_by_test_id(self, test_id):
        self.delay_time()
        try:
            return self.testrail.cases.delete_case(test_id)
        except StatusCodeError:
            return None

    def get_url_by_run_id(self, run_id):
        return testrail_cfg.TESTRAIL_TESTRUN_URL + str(run_id)

    def get_url_by_plan_id(self, plan_id):
        return testrail_cfg.TESTRAIL_TESTPLAN_URL + str(plan_id)

    def delay(self, delay_time=testrail_cfg.TESTRAIL_DELAY_TIME):
        time.sleep(float(delay_time))

    # Update each test case with VFVA format
    def update_vfva_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.update_custom_vavf_sample_by_case_id()
        self.update_custom_vavf_precondition_by_case_id()
        self.update_custom_vavf_expected_dialog_response_by_case_id()
        self.update_custom_vavf_expected_intent_by_case_id()
        self.update_custom_vavf_expected_va_command_by_case_id()

    # Update each test case with VFVA format: English
    def update_vfva_en_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.update_custom_vavf_sample_en_by_case_id()
        self.update_custom_vavf_expected_dialog_response_en_by_case_id()
        self.update_custom_vavf_expected_intent_en_by_case_id()

    # Update each test case with VFVA format
    def update_vfva_test_case_en_to_testrail(self):
        LOGGER.info(self.case_id)
        self.update_custom_vavf_expected_dialog_response_en_by_case_id()

    # Update each test case with normal format
    def update_text_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.__update_title_by_case_id()
        self.__update_custom_preconds_by_case_id()
        self.__update_custom_steps_text_by_case_id()
        self.__update_custom_expected_by_case_id()
        self.__update_custom_note_by_case_id()

    # Update each test case with normal format
    def update_steps_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.__update_title_by_case_id()
        self.__update_custom_preconds_by_case_id()
        self.__update_custom_steps_step_by_case_id()
        self.__update_custom_note_by_case_id()

    # Update each test case with VFVA format: English
    def update_test_case_defect_to_testrail(self):
        LOGGER.info(self.case_id)
        self.__update_custom_defect_by_case_id()

    # Update each test case
    # Test case has format as json
    def update_vss_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.update_custom_vss_command_by_case_id()
        self.update_custom_vavf_precondition_by_case_id()
        self.update_custom_vss_expected_by_case_id()
        self.update_custom_vss_expected_device_by_case_id()

    # Update each test case with normal format
    def update_dialog_template_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.__update_custom_audio_name_by_case_id()
        self.__update_custom_preconds_by_case_id()
        self.__update_custom_audio_name_by_case_id()
        self.__update_custom_sample_by_case_id()
        self.__update_custom_dialog_by_case_id()
        self.__update_custom_dialog_voice_by_case_id()
        self.__update_custom_asr_by_case_id()
        self.__update_custom_entity_by_case_id()
        self.__update_custom_intent_by_case_id()
        self.__update_custom_note_by_case_id()

    # Update each test case with dialog format: language is English
    def update_dialog_template_en_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.__update_custom_sample_en_by_case_id()
        self.__update_custom_dialog_en_by_case_id()

    # Update each test case with dialog format: language is French
    def update_dialog_template_fr_test_case_to_testrail(self):
        LOGGER.info(self.case_id)
        self.__update_custom_sample_fr_by_case_id()
        self.__update_custom_dialog_fr_by_case_id()

    # Update custom_vavf_precondition_by_case_id
    def update_custom_vavf_precondition_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_PRECONDITION.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_PRECONDITION.value])
            if (current_value != update_value) and (update_value != 'None' or update_value != ''):
                LOGGER.info("Current Precondition: " + str(current_value))
                LOGGER.info("Update Precondition : " + str(update_value))
                self.__update_test_case_precondition(custom_vavf_precondition=update_value)
            if (current_value != update_value) and update_value == 'None':
                LOGGER.info("Current Precondition: " + str(current_value))
                LOGGER.info("Update Precondition : " + str(update_value))
                self.__update_test_case_precondition(custom_vavf_precondition='')
        except KeyError:
            LOGGER.info("KeyError: Precondition is invalid!")

    # Update both of custom_vavf_command and title for Vietnamese
    def update_custom_vavf_sample_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_COMMAND.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_COMMAND.value]).lower()
        self.is_correct_language(current_value, update_value)
        if update_value == "None" or update_value == "":
            LOGGER.info("Command is empty! Please check again!")
            os._exit(0)
        if current_value != update_value and update_value != 'None':
            LOGGER.info("Current Command: " + current_value)
            LOGGER.info("Update Command : " + update_value)
            self.__update_test_case_command_vn(custom_vavf_command=update_value)

    # Update both of custom_vavf_command and title
    def update_custom_vavf_sample_en_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_COMMAND_EN.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_COMMAND_EN.value]).lower()
            self.is_correct_language(current_value, update_value)
            if (current_value != update_value) and (update_value != 'None' or update_value != ''):
                LOGGER.info("Current En Command: " + str(current_value))
                LOGGER.info("Update En Command : " + str(update_value))
                self.__update_test_case_command_en(custom_vavf_command_en=update_value)
            if (current_value != update_value) and update_value == 'None':
                LOGGER.info("Current En Command: " + str(current_value))
                LOGGER.info("Update En Command : " + str(update_value))
                self.__update_test_case_command_en(custom_vavf_command_en='')
        except KeyError:
            return None

    # Update both of custom_vavf_command and title
    def update_custom_vss_command_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_COMMAND.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_COMMAND.value])
        if update_value == "None" or update_value == "":
            LOGGER.info("Command is empty! Please check again!")
            os._exit(0)
        if current_value != update_value and update_value != 'None':
            LOGGER.info("Current Command: " + current_value)
            LOGGER.info("Update Command : " + update_value)
            self.__update_test_case_command_vn(custom_vavf_command=update_value)

    # Update expected result
    def update_custom_vss_expected_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_RESPONSE.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_RESPONSE.value])
            if current_value != update_value and (update_value is not 'None' or update_value != ''):
                LOGGER.info("Current Expected: " + current_value)
                LOGGER.info("Update Expected : " + update_value)
                self.update_test_case_custom_vavf_expected(custom_vavf_expected=update_value)
        except KeyError:
            LOGGER.info("KeyError: Expected is invalid!")

    # Update expected result: Vietnamese
    def update_custom_vavf_expected_dialog_response_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_RESPONSE.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_RESPONSE.value])
            if current_value != update_value and (update_value is not 'None' or update_value != ''):
                LOGGER.info("Current Expected: " + current_value)
                LOGGER.info("Update Expected : " + update_value)
                self.update_test_case_custom_vavf_expected(custom_vavf_expected=update_value)
        except KeyError:
            LOGGER.info("KeyError: Expected is invalid!")

    # Update expected result: English
    def update_custom_vavf_expected_en_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_RESPONSE_EN.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_RESPONSE_EN.value])
            self.is_correct_language(current_value, update_value)
            if current_value != update_value and (update_value is not 'None' or update_value != ''):
                LOGGER.info("Current Expected En: " + current_value)
                LOGGER.info("Update Expected En : " + update_value)
                self.update_test_case_custom_vavf_expected_en(custom_vavf_expected_en=update_value)
        except KeyError as error:
            LOGGER.info(error)

    # Update expected intent
    def update_custom_vavf_expected_intent_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_INTENT.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_INTENT.value])
            if current_value != update_value and (update_value != 'None' or update_value != ''):
                LOGGER.info("Current Expected Intent: " + current_value)
                LOGGER.info("Update  Expected Intent: " + update_value)
                self.update_test_case_custom_vavf_expected_intent(custom_vavf_expected_intent=update_value)
        except AttributeError:
            LOGGER.info("KeyError: Expected is invalid!")

    def update_custom_vavf_expected_intent_en_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_INTENT_EN.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_INTENT_EN.value])
            if current_value != update_value and (update_value != 'None' or update_value != ''):
                LOGGER.info("Current Expected Intent En: " + current_value)
                LOGGER.info("Update  Expected Intent En: " + update_value)
                self.update_test_case_custom_vavf_expected_intent_en(custom_vavf_expected_intent_en=update_value)
        except AttributeError:
            LOGGER.info("KeyError: Expected is invalid!")

    def update_custom_vavf_expected_va_command_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_COMMAND.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_COMMAND.value])
            if current_value != update_value and (update_value != 'None' or update_value != ''):
                LOGGER.info("Current Expected VA Command: " + current_value)
                LOGGER.info("Update  Expected VA Command: " + update_value)
                self.update_test_case_custom_vavf_expected_va_command(custom_vavf_expected_command=update_value)
        except AttributeError:
            LOGGER.info("KeyError: Expected is invalid!")

    # Update expected device
    def update_custom_vss_expected_device_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_DEVICE.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_DEVICE.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Expected Device: " + str(current_value))
            LOGGER.info("Update Expected Device : " + str(update_value))
            self.update_test_case_custom_vavf_expected_device(custom_vavf_expected_device=update_value)
        if (current_value != update_value) and update_value == 'None':
            LOGGER.info("Current Expected Device: " + str(current_value))
            LOGGER.info("Update Expected Device : " + str(update_value))
            self.update_test_case_custom_vavf_expected_device(custom_vavf_expected_device='')

    def __update_title_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.TITLE.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.TITLE.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Title: " + str(current_value))
            LOGGER.info("Update Title : " + str(update_value))
            self.update_test_case_title(title=update_value)

    def __update_custom_defect_by_case_id(self):
        current_value = self.current_test_case[TestFields.DEFECT_TESTCASE.value]
        update_value = self.update_test_case[TestFields.DEFECT_TESTCASE.value]
        # if (current_value != update_value) and update_value is not None:
        if (current_value != update_value) and not (str(current_value) == "None" and str(update_value == "")):
        # if (current_value != update_value):
            LOGGER.info("Current Defect: " + str(current_value))
            LOGGER.info("Update Defect : " + str(update_value))
            self.update_test_case_custom_defect(custom_defect=update_value)

    def __update_custom_preconds_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.PRECONDITION.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.PRECONDITION.value])
        if (current_value != update_value) and (update_value is not 'None' or update_value != ''):
            LOGGER.info("Current Precondition: " + str(current_value))
            LOGGER.info("Update Precondition : " + str(update_value))
            self.__update_test_case_custom_preconds(custom_preconds=update_value)
        if (current_value != update_value) and (update_value is 'None' or update_value == ''):
            LOGGER.info("Current Precondition: " + str(current_value))
            LOGGER.info("Update Precondition : " + str(update_value))
            self.__update_test_case_custom_preconds(custom_preconds='')

    def __update_custom_steps_text_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.STEP_TEXT.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.STEP_TEXT.value])
        if (current_value != update_value) and (update_value is not 'None' or update_value != ''):
            LOGGER.info("Current Steps: " + str(current_value))
            LOGGER.info("Update Steps : " + str(update_value))
            self.__update_test_case_custom_steps_text(custom_steps=update_value)
        if (current_value != update_value) and (update_value is not 'None' or update_value != ''):
            LOGGER.info("Current Steps: " + str(current_value))
            LOGGER.info("Update Steps : " + str(update_value))
            self.__update_test_case_custom_steps_text(custom_steps='')

    def __update_custom_steps_step_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.STEP_STEP.value])
        update_value_step = utils.convert_encoding(self.update_test_case[TestFields.STEP_STEP.value])
        update_value_expected = utils.convert_encoding(self.update_test_case[TestFields.STEP_EXPECTED.value])
        list_steps = self.__separate_steps(update_value_step)
        list_expected = self.__separate_steps(update_value_expected)
        update_value = []
        for i in range(len(list_steps)):
            step = list_steps[i]
            expected = list_expected[i]
            content = {TestFields.STEP_SEPERATED_CONTENT.value: step,
                       TestFields.STEP_SEPERATED_EXPECTED.value: expected,
                       TestFields.STEP_SEPERATED_INFO.value: "",
                       TestFields.STEP_SEPERATED_REF.value: ""}
            update_value.append(content)
        if (current_value != str(update_value)) and (str(update_value) != 'None' or str(update_value) != ''):
            LOGGER.info("Current Steps: " + str(current_value))
            LOGGER.info("Update Steps : " + str(update_value))
            self.__update_test_case_custom_steps_step(custom_steps_separated=update_value)

    # Internal used for updating step template
    def __separate_steps(self, text, pattern=r'Step (\d+):([\s\S]*?)(?=Step \d+|$)'):
        steps = re.findall(pattern, text)
        step_list = [step[1].strip() for step in steps]
        return step_list

    def __update_custom_expected_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_RESULT.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_RESULT.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Expected: " + str(current_value))
            LOGGER.info("Update Expected : " + str(update_value))
            self.__update_test_case_custom_expected(custom_expected=update_value)
        if (current_value != update_value) and update_value == 'None':
            LOGGER.info("Current Expected: " + str(current_value))
            LOGGER.info("Update Expected : " + str(update_value))
            self.__update_test_case_custom_expected(custom_expected='')

    def __update_custom_note_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.NOTE.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.NOTE.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Note: " + str(current_value))
            LOGGER.info("Update Note : " + str(update_value))
            self.__update_test_case_custom_note(custom_note=update_value)
        if (current_value != update_value) and update_value == 'None':
            LOGGER.info("Current Note: " + str(current_value))
            LOGGER.info("Update Note : " + str(update_value))
            self.__update_test_case_custom_note(custom_note='')

    def __update_custom_sample_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.DIALOG_SAMPLE.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.DIALOG_SAMPLE.value])
        if update_value == "None" or update_value == "":
            LOGGER.info("Command is empty! Please check again!")
            os._exit(0)
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            # if (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Sample: " + str(current_value))
            LOGGER.info("Update Sample : " + str(update_value))
            self.update_test_case_custom_sample(custom_nlp_sample=update_value)

    # Update sample english for dialog template
    def __update_custom_sample_en_by_case_id(self):
        current_value_vi = utils.convert_encoding(self.current_test_case[TestFields.DIALOG_SAMPLE.value])
        current_value = utils.convert_encoding(self.current_test_case[TestFields.DIALOG_SAMPLE_EN.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.DIALOG_SAMPLE_EN.value])
        self.is_correct_language(current_value, update_value)
        if current_value != update_value and update_value is not None:
            LOGGER.info("Current Sample En: " + current_value)
            LOGGER.info("Update Sample En : " + update_value)
            if current_value_vi is None or current_value_vi == "":
                self.update_test_case_custom_sample_en(custom_nlp_sample_en=update_value, update_title=True)
            else:
                self.update_test_case_custom_sample_en(custom_nlp_sample_en=update_value, update_title=False)

    # Update sample french for dialog template
    def __update_custom_sample_fr_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.DIALOG_SAMPLE_FR.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.DIALOG_SAMPLE_FR.value])
        self.is_correct_language(current_value, update_value)
        if current_value != update_value and update_value is not None:
            LOGGER.info("Current Sample Fr: " + current_value)
            LOGGER.info("Update Sample fr : " + update_value)
            self.update_test_case_custom_sample_fr(custom_nlp_sample_fr=update_value)

    # Update expected result
    def update_custom_vavf_expected_dialog_response_en_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.VA_EXPECTED_RESPONSE_EN.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.VA_EXPECTED_RESPONSE_EN.value])
            self.is_correct_language(current_value, update_value)
            if current_value != update_value and update_value is not None:
                LOGGER.info("Current Expected En: " + current_value)
                LOGGER.info("Update Expected En : " + update_value)
                self.update_test_case_custom_vavf_expected_en(custom_vavf_expected_en=update_value)
        except KeyError as error:
            LOGGER.info(error)

    def __update_custom_audio_name_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.AUDIO_NAME.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.AUDIO_NAME.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Audioname: " + str(current_value))
            LOGGER.info("Update Audioname : " + str(update_value))
            self.update_test_case_custom_audioname(custom_audioname=update_value)

    def __update_custom_intent_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_INTENT.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_INTENT.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Intent: " + str(current_value))
            LOGGER.info("Update Intent : " + str(update_value))
            self.update_test_case_custom_intent(custom_nlp_expected_intent=update_value)

    def __update_custom_note_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.NOTE.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.NOTE.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Note: " + str(current_value))
            LOGGER.info("Update Note : " + str(update_value))
            self.update_test_case_custom_note(custom_note=update_value)

    def __update_custom_domain_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_DOMAIN.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_DOMAIN.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Domain: " + str(current_value))
            LOGGER.info("Update Domain : " + str(update_value))
            self.update_test_case_custom_domain(custom_nlp_expected_domain=update_value)

    def __update_custom_entity_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_ENTITY.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_ENTITY.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Entity: " + str(current_value))
            LOGGER.info("Update Entity : " + str(update_value))
            self.update_test_case_custom_entity(custom_nlp_expected_entity_value=update_value)

    def __update_custom_dialog_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_DIALOG.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_DIALOG.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Dialog: " + str(current_value))
            LOGGER.info("Update Dialog : " + str(update_value))
            self.__update_test_case_custom_dialog(custom_nlp_expected_dialog=update_value)

    def __update_custom_dialog_en_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_DIALOG_EN.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_DIALOG_EN.value])
        self.is_correct_language(current_value, update_value)
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Dialog En: " + str(current_value))
            LOGGER.info("Update Dialog En : " + str(update_value))
            self.__update_test_case_custom_dialog_en(custom_nlp_expected_dialog_en=update_value)

    def __update_custom_dialog_fr_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_DIALOG_FR.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_DIALOG_FR.value])
        self.is_correct_language(current_value, update_value)
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Dialog Fr: " + str(current_value))
            LOGGER.info("Update Dialog Fr : " + str(update_value))
            self.__update_test_case_custom_dialog_fr(custom_nlp_expected_dialog_fr=update_value)

    def __update_custom_dialog_voice_by_case_id(self):
        current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_DIALOG_VOICE.value])
        update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_DIALOG_VOICE.value])
        if (current_value != update_value) and (update_value != 'None' or update_value != ''):
            LOGGER.info("Current Dialog: " + str(current_value))
            LOGGER.info("Update Dialog : " + str(update_value))
            self.__update_test_case_custom_dialog_voice(custom_nlp_expected_dialog_voice=update_value)

    def __update_custom_asr_by_case_id(self):
        try:
            current_value = utils.convert_encoding(self.current_test_case[TestFields.EXPECTED_ASR.value])
            update_value = utils.convert_encoding(self.update_test_case[TestFields.EXPECTED_ASR.value])
            if (current_value != update_value) and (update_value != 'None' or update_value != ''):
                LOGGER.info("Current ASR: " + str(current_value))
                LOGGER.info("Update ASR : " + str(update_value))
                self.__update_test_case_custom_asr(custom_asr_expected=update_value)
        except KeyError:
            return None

    # Add delay time
    def delay_time(self):
        time.sleep(float(testrail_cfg.TESTRAIL_DELAY_TIME))

    # Get all information of test case and result from suite_id
    def get_list_cases_by_suite_id(self, project_id, suite_id, filter=""):
        offset = 0
        size = testrail_cfg.TESTRAIL_MAX_SIZE
        list_test_cases = []
        while size != 0:
            test_cases = self.__get_cases_by_suite_id(project_id, suite_id, offset=offset, filter=filter)
            list_test_cases = list_test_cases + test_cases["cases"]
            size = int(test_cases["size"])
            offset = offset + size
        return list_test_cases

    # Get all information of test case and result from run_id
    def get_list_tests_by_run_id(self, run_id, status_id=None):
        offset = 0
        size = testrail_cfg.TESTRAIL_MAX_SIZE
        list_test_cases = []
        while size != 0:
            test_cases = self.get_tests_by_run_id(run_id, offset=offset, status_id=status_id)
            list_test_cases = list_test_cases + test_cases["tests"]
            size = int(test_cases["size"])
            offset = offset + size
        return list_test_cases

    # Get all information of test case and result from run_id
    def get_list_results_by_run_id(self, run_id, status_id=None):
        offset = 0
        size = testrail_cfg.TESTRAIL_MAX_SIZE
        list_test_results = []
        while size != 0:
            test_results = self.get_results_for_run_by_run_id(run_id, offset=offset, status_id=status_id)
            list_test_results = list_test_results + test_results["results"]
            size = int(test_results["size"])
            offset = offset + size
        return list_test_results

    # Check current and update language
    # Check if different language, if different , end updating
    def is_correct_language(self, current_value, update_value):
        # Same values, no need to check
        if current_value == update_value or current_value in testrail_cfg.TESTRAIL_LANGUAGE_EXCEPT:
            return
        current_value_language = self.language.detect_language_by_sample(current_value)
        update_value_language = self.language.detect_language_by_sample(update_value)
        LOGGER.info(current_value_language)
        LOGGER.info(update_value_language)
        # Same languages, no need to check
        if current_value_language == update_value_language:
            return
        # Different values, need to check
        if self.target_language == testrail_cfg.TESTRAIL_LANGUAGE_EN:
            if current_value_language not in testrail_cfg.TESTRAIL_LANGUAGE_EN_ACCEPT and update_value_language not in testrail_cfg.TESTRAIL_LANGUAGE_EN_ACCEPT:
                LOGGER.info(f"NOTICE: Sample has incorrect language as {self.target_language}, so will not update. "
                            f"Please check again!")
                LOGGER.info(f"Detect language: {update_value}: {current_value_language}")
                raise Exception
        else:
            if current_value_language != update_value_language:
                LOGGER.info(
                    f"NOTICE: Sample has incorrect language as {self.target_language}, so will not update. Please check again!")
                raise Exception

    # Get all information of test case and result from run_id
    def get_list_section_by_project_id(self, project_id):
        list_suites = self.__get_suites_by_project_id(project_id)
        # LOGGER.info(list_suites)
        list_sections = []
        for suite in list_suites:
            offset = 0
            suite_id = suite["id"]
            size = testrail_cfg.TESTRAIL_MAX_SIZE
            while size != 0:
                sections = self.get_sections_by_project_id(project_id, suite_id, offset)
                size = int(sections["size"])
                sections = sections["sections"]
                list_sections = list_sections + sections
                offset = offset + size
        for section in list_sections:
            parent_names = self.get_parent_names_of_section_id(list_sections, section["id"])
            section["section_name"] = utils.convert_list_to_string(parent_names, " > ")
        return list_sections

    # Get parent id and name of each section id
    def get_parent_names_of_section_id(self, data, section_id):
        parent_names = []
        for item in data:
            if item["id"] == section_id:
                parent_id = item["parent_id"]
                if parent_id is not None:
                    parent_names.extend(self.get_parent_names_of_section_id(data, parent_id))
                parent_names.append(item['name'])
                # parent_ids.append((item['id'], item['name']))
                break
        return parent_names

    # # Get list suites in project
    # def get_list_suites_by_project_id(self, project_id):
    #     suites = self.__get_suites_by_project_id(project_id)
    #     list_suites = []
    #     for element in suites:
    #         list_suites.append(element["id"])
    #     return list_suites

    # Get suite_id by project ID and suite name
    def get_suite_id_by_suite_name(self, project_id, suite_name) -> str:
        suites = self.__get_suites_by_project_id(project_id)
        suite = utils.get_elements_in_list_of_dict_equal_value(suites, "name", suite_name)
        suite_id = suite[0]["id"]
        return suite_id

    # Get list defects by test id
    def get_list_defects_by_test_id(self, test_id):
        list_results = self.get_resutls_by_test_id(test_id)
        list_defects = utils.json_extract_by_key(list_results, TestFields.DEFECT.value)
        return list_defects

    # Get list defects by test id
    def get_custom_defect_by_case_id(self, case_id):
        test_result = self.__get_case_by_case_id(case_id)
        return test_result[TestFields.DEFECT_TESTCASE.value]

    # Get list defects by test id
    def get_custom_defect_by_test_id(self, test_id):
        test_result = self.get_test_by_test_id(test_id)
        case_id = test_result["case_id"]
        defect_custom = self.get_custom_defect_by_case_id(case_id)
        return defect_custom

    # Get project name
    def get_run_id_info(self, run_id):
        return self.__get_test_run_info(run_id)

    # Use for VFVA Template
    def add_result_by_test_id_vfva_template(self, test_result):
        if test_result.get(TestFields.TEST_ID.value) is not None:
            self.__add_result_by_test_id_vfva_template(test_result)
        else:
            self.__add_result_by_id_vfva_template(test_result)

    # Use for Text Template
    def add_result_by_test_id_text_template(self, test_result):
        if test_result.get(TestFields.TEST_ID.value) is not None:
            self.__add_result_by_test_id_text_template(test_result)
        else:
            self.__add_result_by_id_text_template(test_result)

    # Add a new section to project_id and get return id
    def add_section_by_project_id(self, project_id, suite_id, section):
        section_info = self.__add_section(project_id, suite_id, section)
        return section_info["id"]

    # Temp function
    def get_section_id_by_case_id(self, case_id):
        # test_case = self.testrail.cases.get_case(case_id=case_id)
        test_case = self.__get_case_by_case_id(case_id)
        return test_case["section_id"]
