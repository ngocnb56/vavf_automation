from mss import mss
import cv2 as cv

class ImageUtils():
    # Capture screen
    def capture_screenshot(self, file_name):
        monitor = 1
        # Capture monitor 1
        with mss() as sct:
            sct.shot(mon=monitor, output=file_name)


    # Compare two images to find similarity metrics
    # Get from : https://www.delftstack.com/howto/python/opencv-compare-images/#:~:text=functions%20of%20OpenCV.-,Use%20the%20norm()%20Function%20of%20OpenCV%20to%20Compare%20Images,pixels%20of%20the%20two%20images.
    def compare_images(self, img1, img2):

        test1 = cv.imread(img1)
        test2 = cv.imread(img2)

        hsv_test1 = cv.cvtColor(test1, cv.COLOR_BGR2HSV)
        hsv_test2 = cv.cvtColor(test2, cv.COLOR_BGR2HSV)

        h_bins = 50
        s_bins = 60
        histSize = [h_bins, s_bins]
        h_ranges = [0, 180]
        s_ranges = [0, 256]
        ranges = h_ranges + s_ranges
        channels = [0, 1]

        hist_test1 = cv.calcHist([hsv_test1], channels, None, histSize, ranges, accumulate=False)
        cv.normalize(hist_test1, hist_test1, alpha=0, beta=1, norm_type=cv.NORM_MINMAX)
        hist_test2 = cv.calcHist([hsv_test2], channels, None, histSize, ranges, accumulate=False)
        cv.normalize(hist_test2, hist_test2, alpha=0, beta=1, norm_type=cv.NORM_MINMAX)

        compare_method = cv.HISTCMP_CORREL

        similarity = cv.compareHist(hist_test1, hist_test2, compare_method)

        print('Similarity = ', similarity)
        return similarity
