#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Chương trình chuyển đổi từ Tiếng Việt có dấu sang Tiếng Việt không dấu
"""

import re
import logging
LOGGER = logging.getLogger(__name__)


class StringUtils:

    # Chuyển từ tiếng Việt có dấu sang không dấu
    def no_accent_vietnamese(self, s):
        s = str(s)
        s = re.sub('[áàảãạăắằẳẵặâấầẩẫậ]', 'a', s)
        s = re.sub('[ÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬ]', 'A', s)
        s = re.sub('[éèẻẽẹêếềểễệ]', 'e', s)
        s = re.sub('[ÉÈẺẼẸÊẾỀỂỄỆ]', 'E', s)
        s = re.sub('[óòỏõọôốồổỗộơớờởỡợ]', 'o', s)
        s = re.sub('[ÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢ]', 'O', s)
        s = re.sub('[íìỉĩị]', 'i', s)
        s = re.sub('[ÍÌỈĨỊ]', 'I', s)
        s = re.sub('[úùủũụưứừửữự]', 'u', s)
        s = re.sub('[ÚÙỦŨỤƯỨỪỬỮỰ]', 'U', s)
        s = re.sub('[ýỳỷỹỵ]', 'y', s)
        s = re.sub('[ÝỲỶỸỴ]', 'Y', s)
        s = re.sub('[-]', 'am_', s)
        s = re.sub('đ', 'd', s)
        s = re.sub('Đ', 'D', s)
        return s

    # Convert unsigned sample to filename
    # e.g bat dieu hoa to bat_dieu_hoa.mp3
    def convert_sample_to_filename_from_dict(self, dict_samples, key_sample='Sample', key_name='Filename'):
        for item in dict_samples:
            sample = item[key_sample]
            filename = self.convert_sample_to_filename(sample)
            item[key_name] = filename
        return dict_samples

    # Convert sample to file name
    def convert_sample_to_filename(self, sample):
        sample = self.no_accent_vietnamese(sample)
        return re.sub(' ', '_', sample) + ".mp3"