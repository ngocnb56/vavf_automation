import subprocess
import time
from datetime import datetime
from subprocess import Popen
from func_timeout import func_timeout, FunctionTimedOut, func_set_timeout
import re
import logging


LOGGER = logging.getLogger(__name__)

# Get log, will fix if any
def log(message):
    print(message)
    # LOGGER.INFO(message)


class CommonUtils():

    # Get special items from dict to list by keys
    def get_item_from_dict_to_list(self, my_dict, key):
        my_list = []
        for item in my_dict:
            sample = item[key]
            my_list.append(sample)
        return my_list

    # Capture log
    def start_capture_log(self, log_name):
        cmd = "adb logcat >> " + log_name
        return subprocess.Popen(cmd, shell=True)

    # Terminate process
    def stop_capture_log(self, process):
        Popen("TASKKILL /F /PID {pid} /T".format(pid=process.pid))

    # Find str in log, exit after [] minutes
    def parse_log(self, log_file, mode):
        file = open(log_file, 'r', encoding='utf-8')
        while True:
            #time.sleep(0.5)
            line = file.readline()
            if line.find(mode) != -1:
                log(line)
                break

    # Timeout after seconds
    def va_wait_for_mode(self, log_file, mode, timeout=5):
        status = True
        try:
            func_timeout(timeout, self.parse_log, args=(log_file, mode))
        except FunctionTimedOut:
            log("parse_log could not complete within 5 seconds and was terminated.\\n")
        return status

    # Get timestamp
    def get_timestamp(self, format="%Y%m%d%H%M%S"):
        # current dateTime
        now = datetime.now()
        # convert to string
        return now.strftime(format)

    # Find string in files
    def check_if_string_in_file(self, file_name, string_to_search):
        log(string_to_search)
        """ Check if any line in the file contains given string """
        # Open the file in read only mode
        with open(file_name, 'r', encoding="utf8") as read_obj:
            # Read all lines in the file one by one
            for line in read_obj:
                # For each line, check if line contains the string
                if string_to_search in line:
                    log(line)
                    return line
        return False

    # Get sub string
    def get_substring(self, full_string, start_with, end_with):
        try:
            found = re.search(start_with + '(.+?)' + end_with, full_string).group(1)
            return found
        except AttributeError:
            # AAA, ZZZ not found in the original string
            return False

