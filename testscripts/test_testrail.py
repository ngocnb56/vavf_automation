from utils.testrail_utils import TestrailUtils

import logging
LOGGER = logging.getLogger(__name__)


class TestTestRail():
    testrail = TestrailUtils()

    def test_import_test_result_to_testrun(self, get_param):
        # Need to update information in testrail.cfg:
        # run_id
        # report_file name
        run_id = get_param["run_id"]
        report_file = get_param["report_file"]
        relative_result = get_param["relative_result"]
        self.testrail.import_test_report_to_testrail(run_id, report_file, relative_result)

    def test_delete_test_case(self):
        list_test_id = [71517, 71534, 71554, 71571, 73996, 74007, 74028, 74039, 74050, 74061, 74069, 74080, 74091, 74097, 74108, 74114, 74125, 74136, 74145, 74152, 74163, 74174, 74185, 74191, 74218, 74302, 74333, 74334, 74344, 74355, 74366, 74747, 74758, 29994, 30135, 30226, 33416, 34135, 34262, 34343, 34445, 34486, 34557, 34618, 10596, 10737, 12632, 12784, 12796, 12802, 12913, 13068, 19463, 19482, 19488, 19494, 19500, 19506, 19512, 19518, 19524, 19530, 19536, 19542, 19548, 19554, 19560, 19566, 19572, 19578, 19584, 19590, 19596, 19602, 19608, 19614, 19620, 19626, 19632, 19638, 19644, 19650, 19656, 19662, 19668, 19674, 19684, 19690, 19696, 19702, 19708, 19714, 19727, 19740, 19747, 19753, 19759, 19765, 19771, 19777, 19790, 19803, 19810, 19820, 19826, 19832, 19839, 19869, 19878, 19899, 19920, 19931, 19941, 19947, 19953, 19959, 19968, 19979, 20000, 20021, 20032, 20036, 20040, 20044, 20060, 20487, 20494, 20501, 20508, 20515, 20521, 20530, 20535, 20541, 20547, 20553, 20559, 20564, 20569, 20574, 20579, 20584, 20589, 20594, 20599, 20606, 20611, 20619, 20623]
        self.testrail.delete_test_cases(list_test_id)

