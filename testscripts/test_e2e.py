import time
from multiprocessing import Process
from utils.testrail_utils import TestrailUtils
from utils.string_utils import StringUtils
from utils.audio_utils import MediaUtils
from utils.utils import CommonUtils
from config.sample import AUDIO_PATH, AUDIO_WAKE_UP, LOG_PATH
from config.testrail import TESTRAIL_RUN_ID
from utils.utils import log
from utils.file_utils import FileUtils
from utils.image_utils import ImageUtils
from enums.va import VAState
from enums.testrail import VAVFTestFields, TestResultStatus
import json

class TestE2E():
    testrail = TestrailUtils()
    my_string = StringUtils()
    media = MediaUtils()
    util = CommonUtils()
    files = FileUtils()
    image = ImageUtils()

    test_id = None
    audio_name = None
    log_name = None
    status = None
    capture_name = None
    test_result = {}
    global AUDIO_WAKE_UP

    # Set full audio name
    def set_test_case(self, test_case_info):
        self.test_id = self.testrail.get_test_id(test_case_info)
        self.command = self.testrail.get_command(test_case_info)
        self.audio_name = AUDIO_PATH + self.my_string.convert_sample_to_filename(self.command)
        self.log_name = LOG_PATH + str(self.test_id) + "_" + self.util.get_timestamp() + ".log"
        self.capture_name = LOG_PATH + str(self.test_id) + "_" + self.util.get_timestamp() + ".png"
        log(self.test_id)
        log(self.audio_name)
        log(self.log_name)
        log(self.capture_name)


    # Play audio and wait state
    def play_audio(self, audio_name, va_state, timeout=5):
        self.media.play_an_audio(audio_name)
        self.status = self.util.va_wait_for_mode(self.log_name, va_state, timeout)

    # Filter log to get result
    def get_test_result(self):
        test_result = {}
        test_result["test_id"] = self.test_id
        test_result["comment"] = "finaltext: " + self.parse_log_by_field(VAState.VA_FINALTEXT.value, '\n') + \
                  "\nva response: " + self.parse_log_by_field(VAState.VA_RESPONSE.value, ', domain=') + \
                  "\nttsmode: " + self.parse_log_by_field(VAState.VA_TTSMODE.value, ',')
        test_result["custom_intent_domain"] = self.parse_log_by_field(VAState.VA_DOMAIN.value, '\n')
        test_result["custom_intent_confidence"] = self.parse_log_by_field(VAState.VA_INTENT.value, '\n')
        test_result["status_id"] =1
        return test_result

    # Parse log
    def parse_log_by_field(self, start_with, end_with):
        found_string = self.util.check_if_string_in_file(self.log_name, start_with)
        if found_string:
            found_string = self.util.get_substring(found_string, start_with, end_with)
        return str(found_string)

    def execute_single_case(self, test_case_info):
        self.set_test_case(test_case_info)
        # Start test case, get log
        proc = self.util.start_capture_log(self.log_name)
        self.play_audio(AUDIO_WAKE_UP, VAState.VA_WAKEUP.value, 5)
        self.play_audio(self.audio_name, VAState.VA_ANSWER.value, 15)
        self.util.stop_capture_log(proc)
        # Capture screen
        self.image.capture_screenshot(self.capture_name)
        self.get_test_result()
        return test_case_info

    def execute_suite_cases(self, list_test_case_info):
        list_results = []
        for i in range(len(list_test_case_info["tests"])):
            log("Wake up mode: Hey Vinfast")
            test_case_info = list_test_case_info["tests"][i]
            test_result = self.execute_single_case(test_case_info)
            #list_results.append(test_case_info)
            self.files.write_to_xls("template.xls", test_result)
        return list_results


    # Test E2E in case need wake up
    def test_suite_case(self):
        list_tests = self.testrail.get_list_tests_by_run_id(TESTRAIL_RUN_ID)
        list_results = self.execute_suite_cases(list_tests)
        list_results = json.loads(list_results)
        #self.files.write_to_xls("template.xls", list_results)
        print(list_results)
        #self.testrail.set_test_result_by_test_id(self.test_id, {"status_id": 1})

    def test_failed_case(self):
        list_tests = self.testrail.get_list_tests_by_run_id(TESTRAIL_RUN_ID, TestResultStatus.FAILED.value)


    def test_untested_case(self):
        list_tests = self.testrail.get_list_tests_by_run_id(TESTRAIL_RUN_ID, TestResultStatus.UNTESTED.value)
        print(list_tests)

    def test_single_case(self):
        self.test_id = "150634"
        test_case_info = self.testrail.get_test_by_test_id(self.test_id)
        log(test_case_info)
        self.execute_single_case(test_case_info)
        log(self.test_result)
        log(self.test_id)
        self.testrail.set_test_result_by_test_id(self.test_id, {"status_id": 1})

