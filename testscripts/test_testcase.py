from utils.string_utils import StringUtils
from utils.file_utils import FileUtils
from config.sample import SAMPLE_FILE, SAMPLE_FILE_NAME
from utils.utils import CommonUtils
import logging
LOGGER = logging.getLogger(__name__)

class TestTestCases():

    my_string = StringUtils()
    files = FileUtils()
    util = CommonUtils()

    def test_convert_sample_name(self):
        number = 100
        print("SAMPLE_FILE: " + SAMPLE_FILE)
        dict_samples = self.files.read_from_xls(SAMPLE_FILE)
        dict_samples = self.my_string.convert_sample_to_filename_from_dict(dict_samples)
        self.files.write_to_xls(SAMPLE_FILE_NAME, dict_samples)


