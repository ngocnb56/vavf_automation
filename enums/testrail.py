from enum import Enum


class TestResultStatus(Enum):
    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))

    PASSED = "1"
    BLOCKED = "2"
    UNTESTED = "2"
    RETEST = "4"
    FAILED = "5"

class VAVFTestFields(Enum):
    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))

    TEST_ID = "test_id"
    TEST_CASE_ID = "id"
    GROUP_COMMAND = "custom_command"
    DESCRIPTION = "custom_vavf_description"
    PRECONDITION = "custom_vavf_precondition"
    COMMAND = "custom_vavf_command"
    EXPECTED_RESULT = "custom_vavf_expected"
    ACTUAL_RESULT = "custom_actual_result"
    RESPONSE = "custom_va_response"
    STATUS = "status_id"
    INTENT = "custom_intent_confidence"
    DOMAIN = "custom_intent_domain"
    TITLE = "title"
    COMMENT = "comment"
