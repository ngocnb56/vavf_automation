from enum import Enum

class VAState(Enum):
    @classmethod
    def list(cls):
        return list(map(lambda c: c.value, cls))

    VA_WAKEUP = "VR_COMMAND_WAKEUP"
    VA_ANSWER = "rawCommand=_command"
    VA_FINALTEXT = "logForTesting finalText: "
    VA_DOMAIN = "onDialogFinished domain: "
    VA_INTENT = "firstIntent: "
    VA_RESPONSE = "answer="
    VA_TTSMODE = "ttsMode: "