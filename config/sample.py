import os
from utils.cfgparse import CfgParse

cfg_parse = CfgParse(file_name=os.getcwd().split('testscripts')[0] + '/sample.cfg')
sample_config = cfg_parse.read_sample_config()

SAMPLE_FILE = sample_config['FILES']['sample']
SAMPLE_FILE_NAME = sample_config['FILES']['sample_name']
AUDIO_PATH = sample_config['AUDIO']['folder']
AUDIO_WAKE_UP = sample_config['AUDIO']['wakeup_audio']
LOG_PATH = sample_config['LOG']['folder']