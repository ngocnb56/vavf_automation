import os

from utils.cfgparse import CfgParse

cfg_parse = CfgParse(file_name=os.getcwd().split('testscripts')[0] + '/testrail.cfg')
testrail_config = cfg_parse.read_testrail_config()

TESTRAIL_DOMAIN_URL = testrail_config['API']['url']
TESTRAIL_USERNAME = testrail_config['API']['email']
TESTRAIL_PASSWORD = testrail_config['API']['password']
TESTRAIL_DOMAIN_URL_TEST_PLAN = testrail_config['DOMAIN']['test_plan']
TESTRAIL_DOMAIN_URL_TEST_RUN = testrail_config['DOMAIN']['test_run']
TESTRAIL_RUN_SUITE_ID = testrail_config['TESTRUN']['suite_id']
TESTRAIL_RUN_ID = testrail_config['TESTRUN']['run_id']
TESTRAIL_TESTREPORT = testrail_config['TESTRESULT']['filename']
NUMBER_OF_IMPORT_TC = testrail_config['TESTRESULT']['number_of_importing']
TEMPLATE_IMPORTING_FILE = testrail_config['TESTRESULT']['template_importing_file']
TEMPLATE_E2E_FILE = testrail_config['TESTRESULT']['template_e2e_file']