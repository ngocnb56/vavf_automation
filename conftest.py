import logging
import os
from datetime import datetime
import config.testrail as testrail_setting
import pytest

driver = None
user_data_path = None
winappdriver = None
download_folder = None
flash_path = None
block_origin_extension_path = None
user_data_default = None
username = None

LOGGER = logging.getLogger(__name__)


def pytest_configure():
    pytest.message = ""


@pytest.mark.hookwrapper
def pytest_runtest_makereport(item):
    """
        Extends the PyTest Plugin to take and embed screenshot in html report right before close webdriver.
        :param request:
        :param item:
        """
    pytest_html = item.config.pluginmanager.getplugin('html')
    outcome = yield
    report = outcome.get_result()
    extra = getattr(report, 'extra', [])

    if report.when == 'call':
        hasattr(report, 'wasxfail')
        timestamp = datetime.now().strftime('%H-%M-%S.%f')[:-3]
        filename = timestamp + ".png"
        if driver is not None and ('winapp' not in item.name):
            try:
                #_capture_screenshot(filename)
                LOGGER.info("Cannot capture screenshot!!!")
            except:
                LOGGER.info("Cannot capture screenshot!!!")

        # if file_name:
        html = '<div><img src="screenshots/%s" style="width:600px;height:228px;" ' \
               'onclick="window.open(this.src)" align="right"/></div>' % filename
        extra.append(pytest_html.extras.html(html))
    report.extra = extra

def pytest_addoption(parser):
    parser.addoption("--run_id", action="store", default=testrail_setting.TESTRAIL_RUN_ID, help="run_id in testrail")
    parser.addoption("--report_file", action="store", default=testrail_setting.TESTRAIL_TESTREPORT, help="report filename")
    parser.addoption("--relative_result", action="store", default=True, help="set relative result")

@pytest.fixture
def get_param(request):
    config_param = {}
    config_param["run_id"] = request.config.getoption("--run_id")
    config_param["report_file"] = request.config.getoption("--report_file")
    config_param["relative_result"] = request.config.getoption("--relative_result")
    return config_param